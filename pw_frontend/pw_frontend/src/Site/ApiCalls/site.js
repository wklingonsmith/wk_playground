
import {getTokenFetch} from '../../Login/LoginStorage';

import { apiWebsitePath } from '../../apipath';

 // TODO: Can I actually do this????

export const fetchSite = async (id, setFunc) => {
    const result =  await fetch(apiWebsitePath + id + '/', {
        method: 'GET',
        mode: 'cors',
        headers: {
        'Content-Type': 'application/json',
        Authorization: getTokenFetch()
        }
    });

    const body = await result.json();

//  Is this best practice? Not sure
    setFunc(body);
  }

/*
export const addSite = async () => {
    await fetch(apiWebsitePath, {
        method: 'POST',
        mode: 'cors',
        headers: {
            'Content-Type': 'application/json',
            Authorization: getTokenFetch()
        },
        body: JSON.stringify({
          link: website.link,
          nickname: website.nickname,
          username: website.username,
          password: website.password,
        })
    })
}
*/

