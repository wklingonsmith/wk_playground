
function getRandomInt(min, max) {
    return Math.floor(min + (Math.random() * (max - min)));
  }

//  TODO: have some sort of settings that user can select the special characters

function generatePassword() {
    const alphabet = "abcdefghijklmnopqrstuvwxyz";
    const numbers = '0123456789';
    const specialChars = "!@#$%^&*()-=_+[]{}";

    const conglomerate = alphabet.toLowerCase() + alphabet.toUpperCase() + numbers + specialChars;
    const characterBank = Array.from(conglomerate);

    const passwordLength = getRandomInt(8, 18);

    let password = '';
    let i, j;

    for (i = 0; i < passwordLength; ++i) {
        j = getRandomInt(0, characterBank.length - 1);

        password += characterBank[j];
    }

    return password;
}

export default generatePassword
