
import { Link } from 'react-router-dom';

import {localSiteAdd, localSiteBills, localSiteOther, localCategory} from '../routerPaths';

function SideNav() {
    const navStyle = {
        color: 'white',
        textDecoration: 'none'
    }

    return (
        <nav className="site_nav">
            <ul>
                <Link to={localSiteBills} style={navStyle}><h4>Bills</h4></Link>
                <Link to={localSiteOther} style={navStyle}><h4>Other Sites</h4></Link>
                <hr />
                <Link to={localSiteAdd} style={navStyle}><h4>Add Site</h4></Link>
                <Link to={localCategory} style={navStyle}><h4>Categories</h4></Link>
            </ul>
        </nav>
        );
    }
    
export default SideNav;
