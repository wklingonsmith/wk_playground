
import SideNav from "./SideNav";

function Websites() {


    return (
        <div className="site">
            <div className="site_home">
                <SideNav />
                <h1>Websites</h1>
            </div>
        </div>
    );
}

export default Websites;
