
import { useState, useEffect } from 'react';
import { Link } from 'react-router-dom';
import '../App.css';

import { getTokenFetch } from '../Login/LoginStorage';
import { apiWebsitePath } from '../apipath'
import { localSiteAdd } from '../routerPaths';

import copyTextToClipboard from './copyTextToClipboard';
import SideNav from './SideNav';

function SiteHome() {

    const [data, setData] = useState([]);
    
    useEffect(() => {
        const fetchItems = async () => {
            try {
                const result = await fetch(apiWebsitePath, {
                    method: 'get',
                    mode: 'cors',
                    headers: {
                        Authorization: getTokenFetch()
                    }
                });
                const body = await result.json();

                setData(body);
            } catch(err) {
                //  TODO:
            }
        }

        fetchItems();
    }, []);

    const openAllSites = () => {
        const links = document.getElementsByClassName('open-link-button');

        for (var i = 0; i < links.length; ++i) {
            links[i].click();
        }
    }
    
    function CopyButton(name, value) {
        return (
            <button onClick={() => copyTextToClipboard(value)}>{ name }</button>
        );
    }

  return (
      <div className="site">
        <div className="site_home">
            <SideNav />
            <h1>Home</h1>
            <button onClick={openAllSites}>Open All Sites</button>
            <br />
            {
                data.map((entry) => (
                    <div key={entry.nickname} className="website-item">
                        <ul>
                            <li><a href={entry.link} target="_blank" rel="noreferrer" className="open-link-button">{entry.nickname}</a></li>
                            <li>{ CopyButton("Username", entry.username) }</li>
                            <li>{ CopyButton("Password", entry.password) }</li>
                            <li>{ <Link to={(localSiteAdd + '/' + entry.id.toString())}><label>Edit</label></Link> }</li>
                        </ul>
                    </div>
                ))
            }
        </div>
      </div>
  );
}

export default SiteHome;
