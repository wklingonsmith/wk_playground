
import { useState } from 'react';
import { useHistory } from 'react-router-dom';

import { getTokenFetch } from '../Login/LoginStorage';

import SideNav from './SideNav';

import { apiCategoryPath } from '../apipath';

function CategoryHome() {
    const [category, setCategory] = useState({id: '', name: '', description: ''});
    const history = useHistory();
    

    const getCategory = (name, description, addCategory) => {
        const handleSubmit = (event) => {    
          const saveItem = async () => {
            try {
              await fetch(apiCategoryPath, {
                method: 'PUT',
                mode: 'cors',
                headers: {
                  'Content-Type': 'application/json',
                  Authorization: getTokenFetch()
                },
                body: JSON.stringify({
                    name: category.name,
                    description: category.description
                })
              });
            } catch(err) {
                //  TODO:
            }
          }
      
          event.preventDefault();
          saveItem();
      
          history.go(0);
        }

        const handleDelete = () => {

        }

        if (addCategory) { 
            return (
                <div className="cat_parent">
                    <form>
                        <ul>
                            <li><input type="text" placeholder="Category Name" className="cat_name"  onChange={e => setCategory(prev => ({...prev, name: e.target.value}))} /></li>
                            <li><input type="text" placeholder="Description of the category" className="cat_desc" onChange={e => setCategory(prev => ({...prev, description: e.target.value}))} /></li>
                            <li><input type="Submit" value="Submit" readOnly={true} /></li>
                        </ul>
                    </form>
                </div>
            );
        }

        return (
            <div className="cat_parent">
                <ul>
                    <li><label className="cat_name">{name}</label></li>
                    <li><label className="cat_desc">{description}</label></li>
                    <li><button className="cat_del" onClick={handleDelete}>Delete</button></li>
                </ul>
                
                
            </div>
        );
    }


    return (
        <div className="site">
            <SideNav />
            <h1>Categories</h1>
            {getCategory('Bills', 'bill stuffs', false)}
            {getCategory('None', 'Nothing', false)}
            {getCategory('', '', true)}
        </div>
    );
}

export default CategoryHome;