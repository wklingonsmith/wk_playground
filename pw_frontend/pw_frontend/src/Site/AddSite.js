
import '../App.css';
import { useState, useEffect } from 'react';
import { useParams, useHistory } from 'react-router-dom';

import { apiWebsitePath } from '../apipath';
import { localSite } from '../routerPaths';

import {getTokenFetch} from '../Login/LoginStorage';

import SideNav from './SideNav';
import generatePassword from './passwordGenerator';


function AddSite() {
  const history = useHistory();
  
  const [website, setWebsite] = useState({id: '', link: '', nickname: '', username: '', password: ''});

  const { id } = useParams();
  const addMode = (!id);

  useEffect(() => {
    const fetchItems = async () => {
      try {
        const result = await fetch(apiWebsitePath + id + '/', {
            method: 'GET',
            mode: 'cors',
            headers: {
              'Content-Type': 'application/json',
              Authorization: getTokenFetch()
            }
        });
        const body = await result.json();

        setWebsite(prev => ({...prev, link: body.link, nickname: body.nickname, username: body.username, password: body.password}));

      } catch(err) {
          //  TODO:
      }
    }

    if (!addMode) {
      fetchItems();
    }

  }, [id, addMode]);

  const handleSubmit = (event) => {    
    const saveItem = async () => {
      const fetchMode = (addMode) ? 'POST' : 'PUT';
      let apiPath = apiWebsitePath;

      if (!addMode) {
        apiPath += id + '/';
      }

      try {
        await fetch(apiPath, {
          method: fetchMode,
          mode: 'cors',
          headers: {
            'Content-Type': 'application/json',
            Authorization: getTokenFetch()
          },
          body: JSON.stringify({
            link: website.link,
            nickname: website.nickname,
            username: website.username,
            password: website.password,
          })
        });
      } catch(err) {
          //  TODO:
      }
    }

    event.preventDefault();
    saveItem();

    history.push(localSite);
    history.go(0);
  }

  const getButtons = (addMode, apiWebsitePath) => {
    let editControls;

    const handleDelete = () => {
      const deleteItem = async () => {
        try {
          await fetch(apiWebsitePath + id + '/', {
            method: 'DELETE',
            mode: 'cors',
            headers: {
              Authorization: getTokenFetch()
            }
          });
        } catch(err) {
          // TODO:
        }
      }
  
      deleteItem();
      history.push(localSite);
      history.go(0);
    }

    if (!addMode) {
      editControls = <input type="button" value="Delete" onClick={handleDelete} />;
    }


    if (addMode) {
      return (
        <input type="submit" value="Submit" />
      )
    }
    else {
      return (
        <div>
          <input type="submit" value="Submit" />
          {editControls}
        </div>
      )
    }
  }

  const setPassword = (event) => {
    const newPassword = generatePassword();

    event.preventDefault();
    setWebsite(prev => ({...prev, password: newPassword}));
  }

  return (
    <div className="site">
      <div className="add_site">
        <SideNav />
        <h1>Add Site</h1>

        <form onSubmit={handleSubmit}>
          <label>
            Website Url
            <br />
            <input type="text" value={website.link} onChange={e => setWebsite(prev => ({...prev, link: e.target.value}))} />
          </label>
          <br />
          <label>
            Website Nickname
            <br />
            <input type="text" value={website.nickname} onChange={e => setWebsite(prev => ({...prev, nickname: e.target.value}))} />
          </label>
          <br />
          <label>
            Username
            <br />
            <input type="text" value={website.username} onChange={e => setWebsite(prev => ({...prev, username: e.target.value}))} />
          </label>
          <br />
          <label>
            Password
            <br />
            <input type="password" value={website.password} onChange={e => setWebsite(prev => ({...prev, password: e.target.value}))} />
            <button onClick={setPassword}>Generate</button>
          </label>
          <br />
          <br />

          {
            getButtons(addMode, apiWebsitePath)
          }

  {
    // TODO: verify user input handling 
    //<Link to='/'><input type="submit" value="Submit" /></Link>
  }
        </form>
      </div>
    </div>
  );
}

export default AddSite;
