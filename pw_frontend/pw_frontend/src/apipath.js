
const basePath = "http://localhost:8000/";
const apiPath = basePath + "api/";

export const apiWebsitePath = apiPath + 'website/';
export const apiCategoryPath = apiPath + 'category/';
export const apiLogin = apiPath + "api-token-auth/";
