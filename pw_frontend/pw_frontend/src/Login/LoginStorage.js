
const loginTokenStorageKey = 'token';

function setLoginToken(value) {
    sessionStorage.setItem(loginTokenStorageKey, value);
}

function getLoginTokenString() {
    const token = sessionStorage.getItem(loginTokenStorageKey);

    if (!token) {
        //TODO: error here?
        return ''
    }

    return token;
}

function getLoginToken() {
    const token = getLoginTokenString();

    if (token) {
        return {
            token: token
        }
    }

}

function getTokenFetch() {
    return "Token " + getLoginTokenString();
}

export {getLoginToken, setLoginToken, getTokenFetch, getLoginTokenString}