
import React, {useState} from 'react';
import PropTypes from 'prop-types';
import { setLoginToken } from './LoginStorage';

import {apiLogin} from '../apipath';

async function loginUser(credentials) {
    try {
        return fetch(apiLogin, {
          method: 'POST',
          mode: 'cors',
          headers: {
            'Content-Type': 'application/json',
          },
          body: JSON.stringify(credentials)
        }).then((data) => {
            return data.json();
        });
      } catch(err) {
          //  TODO:
      }
}


function Login({setToken}, {setTokenSession}) {
    const [username, setUsername] = useState('');
    const [password, setPassword] = useState('');

    const handleSubmit = async (event) => {
        event.preventDefault();
        const token = await loginUser({
            username,
            password
        });
      
        if (!token.non_field_errors) {
            setLoginToken(token.token);
            setToken(token);
        }
      }


    return (
        <form className="login" onSubmit={handleSubmit}>
            <h2>Please Log in!</h2>
            <label>
                <p>Username</p>
                <input type="text" value={username} onChange={e => setUsername(e.target.value)} />
            </label>
            <label>
                <p>Password</p>
                <input type="password" value={password} onChange={e => setPassword(e.target.value)} />
            </label>
            <br />
            <input type="submit" value="Submit" />
        </form>
    )
}

Login.propTypes = {
    setToken: PropTypes.func.isRequired,
}

export default Login