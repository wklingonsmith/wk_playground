
import './App.css';

import { useState } from 'react';
import { BrowserRouter, Switch, Route } from 'react-router-dom';

import NavigationBar from './NavigationBar';
import AddSite from './Site/AddSite';
import SiteHome from './Site/SiteHome';
import CategoryHome from './Site/CategoryHome';
import Websites from './Site/Websites';
import Login from './Login/Login'
import {getLoginToken, setLoginToken} from './Login/LoginStorage'

import { localCategory } from "./routerPaths";


function App() {
  const [token, setToken] = useState(getLoginToken());

  if (!token) {
    return <Login setToken={setToken} setSession={setLoginToken}/>
  }

  return (
    <div className="App">
      <BrowserRouter>
        <NavigationBar />
        <Switch className="app-content">
          <Route path="/" exact component={SiteHome} />
          <Route path="/website" exact component={SiteHome} />
          <Route path="/website/bills" exact component={SiteHome} />
          <Route path="/website/other" exact component={Websites} />
          <Route path="/website/add" exact component={AddSite} />
          <Route path="/website/add/:id" component={AddSite} />
          <Route path={localCategory} component={CategoryHome} />
        </Switch>
      </BrowserRouter>
    </div>
  );
}

export default App;
