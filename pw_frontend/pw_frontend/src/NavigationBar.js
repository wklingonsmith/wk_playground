
import './App.css';

import { Link } from 'react-router-dom';

function NavigationBar() {
    const navStyle = {
        color: 'white',
    }

  return (
    <nav>
        <Link to='/' style={navStyle}><h3>Site Central</h3></Link>
    </nav>

  );
}

export default NavigationBar;
