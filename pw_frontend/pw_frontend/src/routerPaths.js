

export const localSite = "/website";
export const localSiteBills =   localSite + '/bills';
export const localSiteOther =   localSite + '/other';
export const localSiteAdd =     localSite + '/add';

export const localCategory = localSite + '/category';
export const localCategoryAdd = localCategory + '/add';