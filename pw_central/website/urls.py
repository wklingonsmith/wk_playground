from django.urls import path, include
from rest_framework.authtoken.views import obtain_auth_token
from rest_framework import routers
from . import views

router = routers.DefaultRouter()
router.register('api/website', views.WebsiteListCreate, 'website')
router.register('api/category', views.CategoryListCreate, 'category')

urlpatterns = [
    path('', include(router.urls)),
    path('api/api-token-auth/', obtain_auth_token, name='api_token_auth'),
]

#urlpatterns = [
    #path('api/website', views.WebsiteListCreate.as_view())
#]
