from rest_framework import serializers
from .models import Website, Category

class WebsiteSerializer(serializers.ModelSerializer):
    id = serializers.ReadOnlyField()
#    user = serializers.ReadOnlyField()

    class Meta:
        model = Website
        fields = ('id', 'link', 'nickname', 'username', 'password') #, , 'user', 'category'
    

class CategorySerializer(serializers.ModelSerializer):
    id = serializers.ReadOnlyField()

    class Meta:
        model = Category
        fields = ('id', 'name', 'description')