from .models import Website, Category
from .serializers import WebsiteSerializer, CategorySerializer
from rest_framework import viewsets
from rest_framework.permissions import IsAuthenticated

# Create your views here.
class WebsiteListCreate(viewsets.ModelViewSet):

    permission_classes = (IsAuthenticated,)

    serializer_class = WebsiteSerializer

    queryset = Website.objects.all()

    def perform_create(self, serializer):
        serializer.save()
#        serializer.save(user=self.request.user)

#    def get_queryset(self):
#        return Website.objects.filter(user=self.request.user)

class CategoryListCreate(viewsets.ModelViewSet):

    permission_classes = (IsAuthenticated,)
    
    queryset = Category.objects.all()
    serializer_class = CategorySerializer
    
    def perform_create(self, serializer):
        serializer.save()