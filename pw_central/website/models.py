import uuid
from django.db import models
from django.conf import settings


# Create your models here.
class Website(models.Model):
    id = models.AutoField(primary_key=True)
    link = models.TextField()
    nickname = models.TextField()
    username = models.TextField()
    password = models.TextField()
#    user = models.ForeignKey(to=settings.AUTH_USER_MODEL, on_delete=models.SET_NULL, blank=True, null=True)
#    category = models.IntegerField()  // not yet??

class Category(models.Model):
    id = models.AutoField(primary_key=True)
    name = models.TextField()
    description = models.TextField(blank=True)