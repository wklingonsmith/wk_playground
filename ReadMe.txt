Note: this repository is a work in progress

===========================
 pw_central & pw_frontend
   (python 3.9.5)
===========================

Running 'open_django.bat' will open the python virtual environment. From there, just 'cd pw_central' and 
'python manage.py runserver'. The django server should start without issue.

Running 'open_react.bat' will open the python virtual environment. From there, just cd 'pw_frontend' and 
'npm start'. The react server should start without issue.

Once both are running, open a webpage to localhost:3000/ (if it's not already). It'll prompt you to log in.
For convenience, one user is saved into the database.

User: admin
Pass: password

